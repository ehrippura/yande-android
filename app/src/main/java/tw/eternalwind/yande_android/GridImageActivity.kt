/*
 * Copyright 2019 Tzu-Yi Lin
 */
package tw.eternalwind.yande_android

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.android.volley.RequestQueue
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.ImageLoader.ImageContainer
import com.android.volley.toolbox.ImageLoader.ImageListener
import com.android.volley.toolbox.Volley
import com.android.volley.VolleyError
import java.lang.Exception
import java.util.*
import tw.eternalwind.yande.model.PictureSource
import tw.eternalwind.yande.network.CustomImageLoader
import tw.eternalwind.yande.view.AspectImageView
import tw.eternalwind.yande_android.GridImageActivity.GridContentDataSource.CustomViewHolder
import tw.eternalwind.yande_android.Utility.downloadImage
import kotlin.collections.ArrayList

open class GridImageActivity : AppCompatActivity() {
    inner class GridContentDataSource : RecyclerView.Adapter<CustomViewHolder> {
        private var _sources: ArrayList<PictureSource>
        private var _selected: ArrayList<PictureSource>
        private var _queue: RequestQueue
        private var _loader: ImageLoader

        constructor() {
            _sources = ArrayList()
            _selected = ArrayList()
            _queue = Volley.newRequestQueue(applicationContext)
            _loader = CustomImageLoader(_queue)
        }

        constructor(sources: Array<PictureSource>) {
            _sources = sources.toCollection(ArrayList())
            _selected = ArrayList()
            _queue = Volley.newRequestQueue(applicationContext)
            _loader = CustomImageLoader(_queue)
        }

        constructor(sources: ArrayList<PictureSource>) {
            _sources = sources
            _selected = ArrayList()
            _queue = Volley.newRequestQueue(applicationContext)
            _loader = CustomImageLoader(_queue)
        }

        override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CustomViewHolder {
            val v = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.cell_thumbnail, viewGroup, false)
            return CustomViewHolder(v)
        }

        override fun onBindViewHolder(customViewHolder: CustomViewHolder, i: Int) {
            customViewHolder.prepareForReuse()
            val source = _sources[i]
            customViewHolder.setSelect(_selected.contains(source))
            customViewHolder.textView.text = source.fullsize!!.imageSize.toString()
            val aspectRatio = source.fullsize!!.imageSize.height
                .toFloat() / source.fullsize!!.imageSize.width.toFloat()
            customViewHolder.imageView.aspectRatio = aspectRatio
            val container = _loader[source.thumbnail!!.imageUrl, object : ImageListener {
                override fun onResponse(response: ImageContainer, isImmediate: Boolean) {
                    customViewHolder.imageView.setImageBitmap(response.bitmap)
                    customViewHolder.setImageContainer(null)
                }

                override fun onErrorResponse(error: VolleyError) {}
            }]
            customViewHolder.setImageContainer(container)
        }

        override fun getItemCount(): Int {
            return _sources.size
        }

        fun getItem(i: Int): PictureSource {
            return _sources[i]
        }

        val selectedItems: Array<PictureSource>
            get() = _selected.toTypedArray()

        fun addItems(newItems: ArrayList<PictureSource>) {
            val currentSize = itemCount
            _sources.addAll(newItems)
            notifyItemRangeInserted(currentSize, newItems.size)
        }

        fun clearSelection() {
            _selected.clear()
        }

        inner class CustomViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            val textView: TextView
            val imageView: AspectImageView
            private val checkImage: ImageView
            private var _currentContainer: ImageContainer? = null
            fun setImageContainer(container: ImageContainer?) {
                _currentContainer = container
            }

            fun prepareForReuse() {
                if (_currentContainer != null) {
                    _currentContainer!!.cancelRequest()
                    _currentContainer = null
                }
                imageView.setImageDrawable(null)
                setSelect(false)
            }

            fun setSelect(select: Boolean) {
                if (select) {
                    checkImage.visibility = View.VISIBLE
                    itemView.setBackgroundColor(getColor(R.color.colorSelection))
                } else {
                    checkImage.visibility = View.INVISIBLE
                    itemView.setBackgroundColor(getColor(android.R.color.background_dark))
                }
            }

            init {
                v.setOnClickListener { v1: View? ->
                    val index = adapterPosition
                    val s = _sources[index]
                    if (_editing) {
                        if (_selected.contains(s)) {
                            _selected.remove(s)
                            setSelect(false)
                        } else {
                            _selected.add(s)
                            setSelect(true)
                        }
                    } else {
                        val fullscreen =
                            Intent(this@GridImageActivity, FullscreenImageActivity::class.java)
                        fullscreen.putExtra("image_url", s.sample!!.imageUrl)
                        fullscreen.putExtra("image_tags", s.tags)
                        fullscreen.putExtra("fullsize_image_url", s.fullsize!!.imageUrl)
                        startActivity(fullscreen)
                    }
                }
                v.setOnLongClickListener { view: View? ->
                    if (!_editing) {
                        val index = adapterPosition
                        val s = _sources[index]
                        setEditMode(true)
                        _selected.add(s)
                        setSelect(true)
                        return@setOnLongClickListener true
                    }
                    false
                }
                textView = v.findViewById(R.id.textView)
                imageView = v.findViewById(R.id.imageView)
                checkImage = v.findViewById(R.id.checkImageView)
            }
        }
    }

    protected var _gridView: RecyclerView? = null
    protected var _dataSource: GridContentDataSource? = null
    protected var _loadingProgress: ProgressBar? = null
    var isInitialized = false
        private set
    protected var _editing = false
    protected var _displayHome = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_page)
        _loadingProgress = findViewById(R.id.grid_loading_progress)
        _loadingProgress?.visibility = View.GONE
        _gridView = findViewById(R.id.grid_content)
        _gridView?.layoutManager = StaggeredGridLayoutManager(
            2,
            StaggeredGridLayoutManager.VERTICAL
        )
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_download, menu)
        val item = menu.findItem(R.id.download_fullsize)
        item.isVisible = false
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val downloadItem = menu.findItem(R.id.download_fullsize)
        downloadItem.isVisible = _editing
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val itemId = item.itemId
        if (itemId == android.R.id.home) {
            if (_editing) {
                setEditMode(false)
                return true
            }
        } else if (itemId == R.id.download_fullsize) {
            if (!canWriteExternalStorage()) {
                requireAccessExternalStorage()
            } else {
                downloadSelectedImages()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun setDataSource(newDataSource: GridContentDataSource) {
        _dataSource = newDataSource
        _gridView!!.adapter = _dataSource
        this.isInitialized = true
    }

    fun setEditMode(edit: Boolean) {
        val actionBar = supportActionBar
        if (actionBar != null) {
            if (edit) {
                actionBar.setDisplayHomeAsUpEnabled(true)
                actionBar.setHomeAsUpIndicator(android.R.drawable.ic_menu_close_clear_cancel)
            } else {
                actionBar.setDisplayHomeAsUpEnabled(_displayHome)
                actionBar.setHomeAsUpIndicator(null)
                _dataSource!!.clearSelection()
                for (i in 0 until _gridView!!.childCount) {
                    val holder =
                        _gridView!!.getChildViewHolder(_gridView!!.getChildAt(i)) as CustomViewHolder
                    holder.setSelect(false)
                }
            }
        }
        _editing = edit
        invalidateOptionsMenu()
    }

    private fun downloadSelectedImages() {
        val sources = _dataSource!!.selectedItems
        for (source in sources) {
            val image = source.fullsize
            if (image != null) {
                downloadImage(this, image.imageUrl)
            }
        }
        setEditMode(false)
    }

    private fun canWriteExternalStorage(): Boolean {
        val result = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requireAccessExternalStorage() {
        try {
            val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            requestPermissions(permissions, REQUEST_ACTION_DOWNLOAD)
        } catch (e: Exception) {
            Log.i("fullsize", e.localizedMessage)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == REQUEST_ACTION_DOWNLOAD) {
            downloadSelectedImages()
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    companion object {
        private const val REQUEST_ACTION_DOWNLOAD = 0x3
    }
}