package tw.eternalwind.yande_android

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.Menu
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.android.volley.VolleyError
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
import java.lang.Exception
import java.util.ArrayList
import org.json.JSONArray
import tw.eternalwind.yande.model.Pool
import tw.eternalwind.yande.model.Tag
import tw.eternalwind.yande.network.NetworkManager
import tw.eternalwind.yande.network.NetworkManager.Companion.instance
import tw.eternalwind.yande.network.NetworkManager.ResponseHandler
import tw.eternalwind.yande.view.TagSearchResult
import tw.eternalwind.yande.view.TagSearchResult.OnFragmentInteractionListener
import tw.eternalwind.yande_android.APIHelper.searchPool
import tw.eternalwind.yande_android.APIHelper.searchTag

class SearchActivity : AppCompatActivity(), OnTabSelectedListener, OnFragmentInteractionListener {

    private enum class Page(private val code: Int) {
        TAGS(0), POOLS(1);

        fun toInt(): Int = code

        companion object {
            fun from(i: Int): Page {
                return when (i) {
                    1 -> POOLS
                    else -> TAGS
                }
            }
        }
    }

    private var _pager: ViewPager2? = null
    private val _tagResults = arrayOf(TagSearchResult(), TagSearchResult())
    private var _queue: RequestQueue? = null
    private var _searchView: SearchView? = null
    private var _queryText: String? = null
    private var _pools: ArrayList<Pool> = arrayListOf()
    private var _tags: ArrayList<Tag> = arrayListOf()
    private var _currentPage = Page.TAGS

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tags_search)
        _queue = Volley.newRequestQueue(this)
        val tabLayout = findViewById<TabLayout>(R.id.tag_search_tab)
        tabLayout.addOnTabSelectedListener(this)
        _pager = findViewById(R.id.tag_search_pager)

        _pager.let {
            if (it != null) {
                it.adapter = object : FragmentStateAdapter(supportFragmentManager, lifecycle) {
                    override fun createFragment(position: Int): Fragment {
                        return _tagResults[position]
                    }

                    override fun getItemCount(): Int {
                        return _tagResults.size
                    }
                }
                TabLayoutMediator(
                    tabLayout,
                    it,
                    TabConfigurationStrategy { tab: TabLayout.Tab, position: Int ->
                        tab.text = if (position == 0) "TAG" else "POOL"
                    }).attach()
            }
        }
    }

    private fun searchForKey(key: String?) {
        val tag = searchTag(key!!)
        instance.send(
            _queue!!,
            tag,
            NetworkManager.ResponseType.Array,
            object : ResponseHandler {
                override fun response(`object`: Any?, error: VolleyError?) {
                    if (error != null || `object` !is JSONArray) {
                        return
                    }
                    if (_queryText != null && _queryText != key) {
                        return
                    }
                    val list = ArrayList<Tag>()
                    for (i in 0 until `object`.length()) {
                        try {
                            val obj = `object`.getJSONObject(i)
                            val p = Tag.fromJson(obj)
                            list.add(p)
                        } catch (ignored: Exception) {
                        }
                    }
                    setTags(list)
                }
            })
        val pool = searchPool(key)
        instance.send(
            _queue!!,
            pool,
            NetworkManager.ResponseType.Array,
            object : ResponseHandler {
                override fun response(`object`: Any?, error: VolleyError?) {
                    if (error != null || `object` !is JSONArray) {
                        return
                    }
                    if (_queryText != null && _queryText !== key) {
                        return
                    }
                    val list = ArrayList<Pool>()
                    for (i in 0 until `object`.length()) {
                        try {
                            val obj = `object`.getJSONObject(i)
                            val p = Pool.fromJson(obj)
                            list.add(p)
                        } catch (ignored: Exception) {
                        }
                    }
                    setPools(list)
                }
            })
    }

    private fun configureSearchView() {
        _searchView!!.isFocusable = true
        _searchView!!.isIconified = false
        _searchView!!.inputType = InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
        _searchView!!.requestFocusFromTouch()
        _searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                _queryText = newText
                searchForKey(_queryText)
                return false
            }
        })
    }

    private fun setPools(p: ArrayList<Pool>) {
        _pools = p
        val values = p.map {
            it.name
        }.toTypedArray()
        _tagResults[Page.POOLS.toInt()].setValues(values)
    }

    private fun setTags(t: ArrayList<Tag>) {
        _tags = t
        val values = t.map {
            it.name
        }.toTypedArray()
        _tagResults[Page.TAGS.toInt()].setValues(values)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_search, menu)
        val item = menu.findItem(R.id.tag_search)
        _searchView = item.actionView as SearchView
        configureSearchView()
        return true
    }

    /**
     * Tab Selection
     * @param tab tab
     */
    override fun onTabSelected(tab: TabLayout.Tab) {
        _pager!!.currentItem = tab.position
        _currentPage = Page.from(tab.position)
    }

    override fun onTabUnselected(tab: TabLayout.Tab) {}
    override fun onTabReselected(tab: TabLayout.Tab) {}
    override fun onItemSelectedAt(index: Int) {
        when (_currentPage) {
            Page.TAGS -> {
                val tag = _tags[index]
                val intent = Intent(this, TagGridImageActivity::class.java)
                intent.putExtra("search_tag", tag.name)
                startActivity(intent)
            }
            Page.POOLS -> {
                val pool = _pools[index]
                val intent = Intent(this, PoolGridImageActivity::class.java)
                intent.putExtra("pool_id", pool.id)
                intent.putExtra("pool_name", pool.name)
                startActivity(intent)
            }
        }
    }
}