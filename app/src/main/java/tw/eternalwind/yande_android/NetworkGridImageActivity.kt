/*
 * Copyright 2019 Tzu-Yi Lin
 */
package tw.eternalwind.yande_android

import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

abstract class NetworkGridImageActivity : GridImageActivity() {

    @JvmField
    protected var _currentPage = 1

    @JvmField
    protected var _loadComplete = false

    @JvmField
    protected var _loading = false

    @JvmField
    protected var _queue: RequestQueue? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _queue = Volley.newRequestQueue(this)
        _gridView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager = _gridView?.layoutManager as StaggeredGridLayoutManager?
                if (_loading || layoutManager == null) {
                    return
                }
                val itemCount = _dataSource!!.itemCount
                val pos = layoutManager.findLastVisibleItemPositions(null)
                for (p in pos) {
                    if (p > itemCount - 3) {
                        fetchNextPage()
                        break
                    }
                }
            }
        })
    }

    override fun onStart() {
        super.onStart()
        if (!isInitialized) {
            fetchNextPage()
        }
    }

    protected abstract fun fetchNextPage()

    companion object {
        const val MAX_REQUEST_COUNT = 40
    }
}