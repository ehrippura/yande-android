package tw.eternalwind.yande_android

import android.os.Bundle
import android.util.Log
import android.view.View
import com.android.volley.VolleyError
import java.lang.Exception
import java.util.ArrayList
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import tw.eternalwind.yande.model.PictureSource
import tw.eternalwind.yande.model.PictureSource.Companion.fromJSON
import tw.eternalwind.yande.network.NetworkManager
import tw.eternalwind.yande.network.NetworkManager.Companion.instance
import tw.eternalwind.yande.network.NetworkManager.ResponseHandler
import tw.eternalwind.yande_android.APIHelper.getPool

class PoolGridImageActivity : NetworkGridImageActivity() {

    private var _poolId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = intent
        val extra = intent.extras
        if (extra != null) {
            _poolId = extra.getInt("pool_id")
            val poolName = extra.getString("pool_name")
            title = poolName
        }
    }

    override fun fetchNextPage() {

        if (_loadComplete || _queue == null) {
            return
        }

        _loading = true
        _loadingProgress?.visibility = View.VISIBLE

        val api = getPool(_poolId, _currentPage)
        instance.send(_queue!!, api, NetworkManager.ResponseType.Object, object : ResponseHandler {

            override fun response(`object`: Any?, error: VolleyError?) {

                _loadingProgress?.visibility = View.GONE

                if (error != null || `object` !is JSONObject) {
                    _loading = false
                    return
                }

                val array: JSONArray
                try {
                    array = `object`.getJSONArray("posts")
                } catch (ex: Exception) {
                    Log.i("pool", ex.localizedMessage ?: "Unkonwn Error")
                    _loadComplete = true
                    _loading = false
                    return
                }

                if (array.length() == 0) {
                    _loadComplete = true
                    _loading = false
                    return
                }

                val sources = ArrayList<PictureSource>(array.length())
                try {
                    for (i in 0 until array.length()) {
                        val obj = array.getJSONObject(i)
                        try {
                            val source = fromJSON(obj)
                            sources.add(source)
                        } catch (ignore: Exception) {
                        }
                    }
                    _currentPage += 1
                    if (!isInitialized) {
                        val dataSource = GridContentDataSource(sources)
                        setDataSource(dataSource)
                    } else {
                        _dataSource?.addItems(sources)
                    }
                } catch (exception: JSONException) {
                    Log.i("network", exception.localizedMessage ?: "Unknown Error")
                }

                _loading = false
            }
        })
    }
}