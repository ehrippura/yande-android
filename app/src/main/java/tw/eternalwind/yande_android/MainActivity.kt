/*
 * Copyright 2019 Tzu-Yi Lin
 */
package tw.eternalwind.yande_android

import android.content.Intent
import android.view.Menu
import android.view.MenuItem

class MainActivity : TagGridImageActivity() {

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main_page, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val searchItem = menu.findItem(R.id.tag_search)
        searchItem.isVisible = !_editing
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val itemId = item.itemId
        if (itemId == R.id.tag_search) {
            val searchIntent = Intent(this, SearchActivity::class.java)
            startActivity(searchIntent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}