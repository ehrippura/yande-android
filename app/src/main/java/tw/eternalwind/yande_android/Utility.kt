package tw.eternalwind.yande_android

import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import java.io.UnsupportedEncodingException
import java.net.URLDecoder

object Utility {
    @JvmStatic
    fun downloadImage(context: Context, url: String) {

        val manager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        val req = DownloadManager.Request(Uri.parse(url))
        var filename = url.substring(url.lastIndexOf("/") + 1)

        filename = try {
            URLDecoder.decode(filename, "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            return
        }

        req.setTitle("yande download")
        req.setDescription(filename)
        req.addRequestHeader("User-Agent", "yande-client " + BuildConfig.VERSION_NAME)

        val newName: String = if (filename.length > 127) {
            val extension = filename.substring(filename.lastIndexOf(".") + 1)
            filename.substring(0, 127 - extension.length - 1) + "." + extension
        } else {
            filename
        }

        req.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, newName)
        manager.enqueue(req)
    }
}