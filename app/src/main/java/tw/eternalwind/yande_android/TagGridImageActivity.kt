package tw.eternalwind.yande_android

import android.os.Bundle
import android.util.Log
import android.view.View
import com.android.volley.VolleyError
import org.json.JSONArray
import org.json.JSONException
import tw.eternalwind.yande.model.PictureSource
import tw.eternalwind.yande.model.PictureSource.Companion.fromJSON
import tw.eternalwind.yande.network.NetworkManager
import tw.eternalwind.yande.network.NetworkManager.Companion.instance
import tw.eternalwind.yande.network.NetworkManager.ResponseHandler
import tw.eternalwind.yande_android.APIHelper.getPosts
import java.util.*

open class TagGridImageActivity : NetworkGridImageActivity() {

    private var _searchTag: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = intent
        val extra = intent.extras
        if (extra != null) {
            _searchTag = extra.getString("search_tag")
            _displayHome = true
            if (_searchTag != null && _searchTag!!.isNotEmpty()) {
                title = _searchTag
            }
        }
    }

    override fun fetchNextPage() {

        if (_loadComplete) {
            return
        }

        _loading = true
        _loadingProgress?.visibility = View.VISIBLE

        val api = getPosts(_currentPage, MAX_REQUEST_COUNT, _searchTag)

        instance.send(_queue!!, api, NetworkManager.ResponseType.Array, object : ResponseHandler {

            override fun response(`object`: Any?, error: VolleyError?) {

                _loadingProgress?.visibility = View.GONE

                if (error != null || `object` !is JSONArray) {
                    _loading = false
                    return
                }

                if (`object`.length() == 0) {
                    _loadComplete = true
                    _loading = false
                    return
                }

                val sources = ArrayList<PictureSource>(`object`.length())

                try {
                    for (i in 0 until `object`.length()) {
                        val obj = `object`.getJSONObject(i)
                        try {
                            val source = fromJSON(obj)
                            sources.add(source)
                        } catch (ignore: Exception) {
                        }
                    }
                    _currentPage += 1
                    if (!isInitialized) {
                        val dataSource = GridContentDataSource(sources)
                        setDataSource(dataSource)
                    } else {
                        _dataSource?.addItems(sources)
                    }
                } catch (exception: JSONException) {
                    Log.i("network", exception.localizedMessage ?: "" )
                }

                _loading = false
            }
        })
    }
}