/*
 * Copyright 2019 Tzu-Yi Lin
 */
package tw.eternalwind.yande_android

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.VolleyError
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.ImageLoader.ImageContainer
import com.android.volley.toolbox.ImageLoader.ImageListener
import com.android.volley.toolbox.Volley
import java.io.UnsupportedEncodingException
import java.lang.Exception
import java.net.URLDecoder
import tw.eternalwind.yande.network.CustomImageLoader
import tw.eternalwind.yande_android.Utility.downloadImage

class FullscreenImageActivity : AppCompatActivity() {

    private var _fullsizeImageUrl: String? = null
    private var _imageView: ImageView? = null
    private var _loadingProgress: ProgressBar? = null
    private var _container: ImageContainer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_fullscreen_image)

        val networkQueue = Volley.newRequestQueue(this)
        val imageLoader: ImageLoader = CustomImageLoader(networkQueue
        )
        _imageView = findViewById(R.id.full_image_view)
        _loadingProgress = findViewById(R.id.sample_loading_progress)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val extraInfo = intent.extras

        if (extraInfo != null) {
            val url = extraInfo.getString("image_url")
            val tags = extraInfo.getStringArray("image_tags")
            _fullsizeImageUrl = extraInfo.getString("fullsize_image_url")
            _loadingProgress?.visibility = View.VISIBLE

            _container = imageLoader[url, object : ImageListener {
                override fun onResponse(response: ImageContainer, isImmediate: Boolean) {
                    val bitmap = response.bitmap
                    if (bitmap != null) {
                        _loadingProgress?.visibility = View.GONE
                        _imageView?.setImageBitmap(bitmap)
                    }
                }

                override fun onErrorResponse(error: VolleyError) {
                    _loadingProgress?.visibility = View.GONE
                    Log.i("fullscreen", error.localizedMessage ?: "unknown error")
                }
            }]

            val filename = url?.substring(url.lastIndexOf("/") + 1)
            if (filename != null) {
                title = try {
                    URLDecoder.decode(filename, "UTF-8")
                } catch (e: UnsupportedEncodingException) {
                    filename
                }
            }
        }
    }

    override fun onDestroy() {
        if (_container?.bitmap == null) {
            _container?.cancelRequest()
        }
        super.onDestroy()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_download, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.download_fullsize) {
            if (!canWriteExternalStorage()) {
                requireAccessExternalStorage()
            } else {
                downloadImage(this, _fullsizeImageUrl!!)
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun canWriteExternalStorage(): Boolean {
        val result = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requireAccessExternalStorage() {
        try {
            val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            requestPermissions(permissions, REQUEST_ACTION_DOWNLOAD)
        } catch (e: Exception) {
            Log.i("fullsize", e.localizedMessage ?: "unknown error")
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == REQUEST_ACTION_DOWNLOAD) {
                downloadImage(this, _fullsizeImageUrl!!)
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    companion object {
        private const val REQUEST_ACTION_DOWNLOAD = 0x3
    }
}