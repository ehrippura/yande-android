/*
 * Copyright 2019 Tzu-Yi Lin
 */
package tw.eternalwind.yande_android

import tw.eternalwind.yande.network.API
import tw.eternalwind.yande.network.Resources

object APIHelper {
    @JvmStatic
    fun getPosts(page: Int, limit: Int, tagName: String?): API {
        val api = API(Resources.API.postSearchPath)
        api.addParameter("page", page.toString())
        api.addParameter("limit", limit.toString())
        if (tagName != null) {
            api.addParameter("tags", tagName)
        }
        return api
    }

    @JvmStatic
    fun searchPool(key: String): API {
        val api = API(Resources.API.poolSearchPath)
        api.addParameter("query", key)
        return api
    }

    @JvmStatic
    fun searchTag(key: String): API {
        val api = API(Resources.API.tagSearchPath)
        api.addParameter("name", key)
        api.addParameter("order", "count")
        api.addParameter("limit", "150")
        return api
    }

    @JvmStatic
    fun getPool(id: Int, page: Int): API {
        val api = API(Resources.API.poolListPath)
        api.addParameter("id", id.toString())
        api.addParameter("page", page.toString())
        return api
    }
}