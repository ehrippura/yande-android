/*
 * Copyright 2019 Tzu-Yi Lin
 */
package tw.eternalwind.yande.network

import android.graphics.Bitmap
import android.widget.ImageView.ScaleType
import com.android.volley.*
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.ImageRequest
import tw.eternalwind.yande.model.ImageCache.Companion.instance
import tw.eternalwind.yande_android.BuildConfig
import java.util.*

class CustomImageLoader(queue: RequestQueue?) : ImageLoader(queue, instance) {
    override fun makeImageRequest(requestUrl: String, maxWidth: Int, maxHeight: Int, scaleType: ScaleType, cacheKey: String): Request<Bitmap> {
        return object : ImageRequest(
                requestUrl,
                Response.Listener { response: Bitmap? -> onGetImageSuccess(cacheKey, response) },
                maxWidth,
                maxHeight,
                scaleType,
                Bitmap.Config.ARGB_8888,
                Response.ErrorListener { error: VolleyError? -> onGetImageError(cacheKey, error) }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val header = HashMap<String, String>()
                header["User-Agent"] = "yande-client " + BuildConfig.VERSION_NAME
                return header
            }
        }
    }
}