/*
 * Copyright 2019 Tzu-Yi Lin
 */
package tw.eternalwind.yande.network

interface RequestConvertable {
    fun asRequest(): NetworkRequest
}
