/*
 * Copyright 2019 Tzu-Yi Lin
 */
package tw.eternalwind.yande.network

import java.util.*

class API(path: String) : RequestConvertable {

    private var target: String
    private val parameters: HashMap<String, String>

    fun addParameter(key: String, value: String) {
        parameters[key] = value
    }

    override fun asRequest(): NetworkRequest {
        val r = NetworkRequest(target)
        val allKeys: Set<String> = parameters.keys
        for (key in allKeys) {
            val value = parameters[key]
            val q = Query(key, value)
            r.addQuery(q)
        }
        return r
    }

    init {
        val base = NetworkManager.instance.baseUrl
        target = "$base/$path"
        parameters = HashMap()
    }
}