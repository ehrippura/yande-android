/*
 * Copyright 2019 Tzu-Yi Lin
 */
package tw.eternalwind.yande.network

class Query {

    var tag: String? = null

    var value: String? = null

    constructor()

    constructor(tag: String?, value: String?) {
        this.tag = tag
        this.value = value
    }
}
