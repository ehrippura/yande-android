/*
 * Copyright 2019 Tzu-Yi Lin
 */
package tw.eternalwind.yande.network

object Resources {
    object API {
        const val postSearchPath = "post.json"
        const val tagSearchPath = "tag.json"
        const val poolSearchPath = "pool.json"
        const val poolListPath = "pool/show.json"
    }
}
