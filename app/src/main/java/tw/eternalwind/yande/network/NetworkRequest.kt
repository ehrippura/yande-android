/*
 * Copyright 2019 Tzu-Yi Lin
 */
package tw.eternalwind.yande.network

import android.net.Uri
import java.util.*

class NetworkRequest(private val url: String) {

    enum class Method {
        GET, POST
    }

    private val queryItems: ArrayList<Query> = ArrayList()

    var timeoutInterval = 60.0

    var httpBody: ByteArray? = null

    var method = Method.GET

    fun addQuery(query: Query) {
        queryItems.add(query)
    }

    fun getUrl(): String {
        if (queryItems.isEmpty()) {
            return url
        }
        val builder = Uri.parse(url).buildUpon()
        for (q in queryItems) {
            builder.appendQueryParameter(q.tag, q.value)
        }
        return builder.toString()
    }
}