/*
 * Copyright 2019 Tzu-Yi Lin
 */
package tw.eternalwind.yande.network

import android.util.Log
import com.android.volley.*
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONArray
import org.json.JSONObject
import tw.eternalwind.yande_android.BuildConfig
import java.util.*

class NetworkManager private constructor() {

    enum class ResponseType {
        Object, Array
    }

    interface ResponseHandler {
        fun response(`object`: Any?, error: VolleyError?)
    }

    var baseUrl = "https://yande.re"

    fun send(queue: RequestQueue, conv: RequestConvertable, type: ResponseType, handler: ResponseHandler) {
        val networkRequest = conv.asRequest()
        when (type) {
            ResponseType.Array -> sendArrayRequest(queue, networkRequest, handler)
            ResponseType.Object -> sendObjectRequest(queue, networkRequest, handler)
        }
    }

    private fun sendArrayRequest(queue: RequestQueue, networkRequest: NetworkRequest, handler: ResponseHandler) {

        val method: Int = when (networkRequest.method) {
            NetworkRequest.Method.POST -> Request.Method.POST
            NetworkRequest.Method.GET -> Request.Method.GET
        }

        val req: JsonArrayRequest = object : JsonArrayRequest(method, networkRequest.getUrl(), null, Response.Listener { jsonArray: JSONArray? -> handler.response(jsonArray, null) }, Response.ErrorListener { error: VolleyError ->
            Log.d("Network", error.localizedMessage ?: "")
            handler.response(null, error)
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val header = HashMap<String, String>()
                header["User-Agent"] = "yande-client " + BuildConfig.VERSION_NAME
                return header
            }
        }

        req.retryPolicy = DefaultRetryPolicy(
                java.lang.Double.valueOf(networkRequest.timeoutInterval * 1000).toInt(),
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        queue.add(req)
    }

    private fun sendObjectRequest(queue: RequestQueue, networkRequest: NetworkRequest, handler: ResponseHandler) {

        val method: Int = when (networkRequest.method) {
            NetworkRequest.Method.POST -> Request.Method.POST
            NetworkRequest.Method.GET -> Request.Method.GET
        }

        val req: JsonObjectRequest = object : JsonObjectRequest(
            method,
            networkRequest.getUrl(),
            null,
            Response.Listener { jsonObject: JSONObject? -> handler.response(jsonObject, null) },
            Response.ErrorListener { error: VolleyError ->
                Log.d("Network", error.localizedMessage ?: "")
                handler.response(null, error)
            }
        ) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val header = HashMap<String, String>()
                header["User-Agent"] = "yande-client " + BuildConfig.VERSION_NAME
                return header
            }
        }

        req.retryPolicy = DefaultRetryPolicy(
            (networkRequest.timeoutInterval * 1000).toInt(),
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        queue.add(req)
    }

    companion object {
        @JvmStatic
        var instance = NetworkManager()
    }
}