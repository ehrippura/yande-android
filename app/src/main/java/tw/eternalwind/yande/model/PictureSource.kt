/*
 * Copyright 2019 Tzu-Yi Lin
 */

package tw.eternalwind.yande.model

import android.util.Size

import org.json.JSONException
import org.json.JSONObject

class PictureSource internal constructor(id: Int, name: String) : YandeItem(id, name) {

    var tags: Array<String>? = null
        private set

    var fullsize: Image? = null
        private set

    var sample: Image? = null
        private set

    var thumbnail: Image? = null
        private set

    companion object {

        @Throws(JSONException::class)
        fun fromJSON(json: JSONObject): PictureSource {

            val id = json.getInt("id")
            val pictureSource = PictureSource(id, id.toString())

            var width = json.getInt("preview_width")
            var height = json.getInt("preview_height")
            pictureSource.thumbnail = Image(json.getString("preview_url"), Size(width, height))

            width = json.getInt("sample_width")
            height = json.getInt("sample_height")
            pictureSource.sample = Image(json.getString("sample_url"), Size(width, height))

            width = json.getInt("width")
            height = json.getInt("height")
            pictureSource.fullsize = Image(json.getString("file_url"), Size(width, height))

            val tagsString = json.optString("tags")
            if (tagsString.isNotEmpty()) {
                val tags = tagsString.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                pictureSource.tags = tags
            }

            return pictureSource
        }
    }
}
