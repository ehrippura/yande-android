package tw.eternalwind.yande.model

import org.json.JSONException
import org.json.JSONObject

class Tag internal constructor(id: Int, name: String, count: Int) : ImageAnnotation(id, name, count) {
    companion object {
        @Throws(JSONException::class)
        fun fromJson(`object`: JSONObject): Tag {
            val id = `object`.getInt("id")
            val name = `object`.getString("name")
            val count = `object`.getInt("count")
            return Tag(id, name, count)
        }
    }
}
