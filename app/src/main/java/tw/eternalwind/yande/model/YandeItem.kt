package tw.eternalwind.yande.model

open class YandeItem internal constructor(val id: Int, val name: String)
