package tw.eternalwind.yande.model

open class ImageAnnotation internal constructor(id: Int, name: String, val imageCount: Int) : YandeItem(id, name)
