package tw.eternalwind.yande.model

import org.json.JSONObject

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.SimpleTimeZone

class Pool internal constructor(id: Int, name: String, count: Int) : ImageAnnotation(id, name, count) {

    var createTime: Date? = null
        private set
    val description: String? = null

    companion object {

        @Throws(Exception::class)
        fun fromJson(`object`: JSONObject): Pool {
            val id = `object`.getInt("id")
            val name = `object`.getString("name")
            val count = `object`.getInt("post_count")

            val p = Pool(id, name, count)

            val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
            df.timeZone = SimpleTimeZone(0, "UTC")

            val createString = `object`.getString("created_at")
            p.createTime = df.parse(createString)

            return p
        }
    }
}
