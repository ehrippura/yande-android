/*
 * Copyright 2019 Tzu-Yi Lin
 */

package tw.eternalwind.yande.model

import android.util.Size

class Image internal constructor(var imageUrl: String, var imageSize: Size)
