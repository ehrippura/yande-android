/*
 * Copyright 2019 Tzu-Yi Lin
 */

package tw.eternalwind.yande.model

import android.graphics.Bitmap
import android.util.LruCache

import com.android.volley.toolbox.ImageLoader

class ImageCache private constructor() : LruCache<String, Bitmap>(defaultLruCacheSize), ImageLoader.ImageCache {

    override fun getBitmap(url: String): Bitmap? {
        return get(url)
    }

    override fun putBitmap(url: String, bitmap: Bitmap) {
        put(url, bitmap)
    }

    companion object {

        val instance = ImageCache()

        val defaultLruCacheSize: Int
            get() {
                val maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()
                return maxMemory / 8
            }
    }
}
