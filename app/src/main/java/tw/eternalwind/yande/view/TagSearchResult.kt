package tw.eternalwind.yande.view

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import tw.eternalwind.yande_android.R
import android.widget.TextView
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import java.lang.RuntimeException

class TagSearchResult : Fragment() {
    /**
     * Delegate definition
     */
    interface OnFragmentInteractionListener {
        fun onItemSelectedAt(index: Int)
    }

    /**
     * Data Source
     */
    private inner class DataSource : RecyclerView.Adapter<ViewHolder>() {
        private var _itemValues = arrayOf<String>()
        override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
            val v = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.cell_tag, viewGroup, false)
            return ViewHolder(v)
        }

        override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
            val value = _itemValues[i]
            viewHolder.label.text = value
        }

        override fun getItemCount(): Int {
            return _itemValues.size
        }

        var itemValues: Array<String>
            set(value) {
                _itemValues = value
                notifyDataSetChanged()
            }
            get() = _itemValues
    }

    /**
     * View Holder
     */
    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val label: TextView = itemView.findViewById(R.id.label)

        init {
            itemView.setOnClickListener { v: View? ->
                if (_listener != null) {
                    _listener!!.onItemSelectedAt(adapterPosition)
                }
            }
        }
    }

    private val _dataSource: DataSource = DataSource()

    private var _tags: List<String> = listOf()

    private var _listener: OnFragmentInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val contentView = inflater.inflate(R.layout.fragment_tag_search_result, container, false)
        val listView: RecyclerView = contentView.findViewById(R.id.tag_recycle_view)
        listView.layoutManager = GridLayoutManager(activity, 2)
        listView.adapter = _dataSource
        listView.addOnScrollListener(
            object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy == 0) {
                        return
                    }
                    val activity = activity
                    if (activity != null) {
                        val inputMethodManger =
                            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                        inputMethodManger.hideSoftInputFromWindow(recyclerView.windowToken, 0)
                    }
                }
            },
        )
        return contentView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        _listener = if (context is OnFragmentInteractionListener) {
            context
        } else {
            throw RuntimeException(
                context.toString()
                        + " must implement OnFragmentInteractionListener"
            )
        }
    }

    override fun onDetach() {
        _listener = null
        super.onDetach()
    }

    fun setValues(values: Array<String>) {
        _dataSource.itemValues = values
    }
}